import urllib
import re
from bs4 import BeautifulSoup
from inspect import getsourcefile
import sys
import os
from urllib.parse import urlparse
import urllib.request
import mysql.connector
import yaml

url = 'https://www.megapixel.cz/bazar-digitalni-fotoaparaty-videokamery?b%5B6%5D=nikon'
o = urlparse(url)

db_url = o.scheme+"://"+o.netloc

category = 'Megapixel fotoaparaty'

with urllib.request.urlopen(url) as response:
   html = response.read()

db_url_single = "";
db_title = "";
db_content = "";
db_price = "";


current_path = os.path.abspath(getsourcefile(lambda:0))
current_dir = os.path.dirname(current_path)
parent_dir = current_dir[:current_dir.rfind(os.path.sep)]

conf = yaml.load(open(parent_dir+'/conf/application.yml'))

secret_user = conf['aws']['megapixel']['username']
secret_password = conf['aws']['megapixel']['password']
secret_database = conf['aws']['megapixel']['database']
secret_host = conf['aws']['megapixel']['host']
secret_port = conf['aws']['megapixel']['port']

cnx = mysql.connector.connect(user=secret_user, password=secret_password, database=secret_database, host=secret_host, port=secret_port)

cursor = cnx.cursor()
query = "INSERT INTO megapixel.Record (url, title, content, price, category) VALUES (%s, %s, %s, %s, %s)"

soup = BeautifulSoup(html, "html.parser")
l = soup.find(id='product-list-holder')
content = l.find_all("div",class_="spc")

for i in content:
	t = i.find('span',{'class':'name'})
	db_title = t.get_text()
	db_url_single = db_url+i.h2.a['href']
	db_content = i.find('span',{'class':'perex'}).get_text()
	db_price = i.find('p',{'class':'price'}).get_text()
	db_price = re.findall(r"\d+",db_price)
	db_price = ''.join(db_price)

	query1 = "SELECT * FROM megapixel.Record WHERE URL = '"+db_url_single+"';"
	cursor.execute(query1)
	row = cursor.fetchall()

	if len(row) == 0:
		cursor.execute(query, (str(db_url_single), str(db_title), str(db_content), str(db_price), str(category)))
		cnx.commit()

cursor.close()
cnx.close()
